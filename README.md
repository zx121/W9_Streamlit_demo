# Streamlit_demo
Streamlit App with a Hugging Face Model

## Requirements

- Create a website using Streamlit
- Connect to an open source LLM (Hugging Face)
- Deploy model via Streamlit or other service (accessible via browser)


## Functioning Web App
This streamlit app is trying to build a chatbot with open-source LLM from huggingface. It mainly supports the functionality:
- Chatting with LLM 
- Remember and clear chat history
- Stream format to give the response
- **Keep model cache so that it is only need to load once**.

![Stream](image.png)

![alt text](image-7.png)

## Connection to Open Source LLM
I use `AutoModelForCausalLM.from_pretrained` and `AutoTokenizer.from_pretrained` to import LLM and the corresponding tokenizer. This is a kind of LLM trained with Chinese and English.

```python
@st.cache_resource
def init_model():
    model_path = "baichuan-inc/Baichuan-13B-Chat"
    model = AutoModelForCausalLM.from_pretrained(
        model_path, torch_dtype=torch.float16, device_map="auto", trust_remote_code=True
    )
    model.generation_config = GenerationConfig.from_pretrained(model_path)
    tokenizer = AutoTokenizer.from_pretrained(
        model_path, use_fast=False, trust_remote_code=True
    )
    return model, tokenizer
```
![alt text](image-6.png)

![alt text](image-1.png)

## Chatbot Performance
I deployed the APP on AWS EC2 with `g5.x2large`. As for the generation speed, it is much better compared with plain CPU machines, but it is still a little bit slow than popular online chat LLM such as ChatGPT. However, as for content, it is pretty much good enough. 
![alt text](image-2.png) 


## Hosting EC2 or provider outside of Streamlit 
Using command `streamlit run wewb_demo.py`, the streamlit will provide the service on 8501 port. You just need to configure your EC2 network security group to expose 8501 port. And then, you can access the LLM service with `EC2 Public IP:Port`.
![alt text](image-5.png)

![alt text](image-3.png)

![alt text](image-4.png)

## Debugging process
During the inference, we may encounter the warning like "WARNING:root:Some parameters are on the meta device device because they were offloaded to the cpu." It is because our machine GPU is not powerful enough.

You can choose different ways to solve it:
1. Use torch: In Python environment, using `torch.cuda.empty_cache()` can clean the GPU memory.
2. Use command: 
```bash
nvidia-smi --gpu-reset   # Reset GPU status to release memry 
nvidia-smi --query-gpu=memory.used --format=csv   # Show GPU memory situation
```